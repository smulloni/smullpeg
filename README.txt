Smull-Peg README
================

This little game is a computer version of a familiar peg game that
would be wrong to explain here, as that would leave you with nothing
to figure out by yourself.

Were you, upon launching this program (via the "smullpeg" script), or
perhaps some seconds afterwards, to remark that it is somewhat lacking
in visual flair, I would respond, "Tchah -- those deaf to nostalgia
need not apply".

If you are impatient to know immediately when you have made a move
that dooms you to ignominious failure, press T (for hinT) to enter
hint mode, and when you make a bad move the computer will groan in
disappointment.  

REQUIREMENTS
------------

Python 2.5 and PyGame are required.

LICENSE
-------

The GPL license, version 2 or later, applies.

© 2009 Jacob Smullyan.
