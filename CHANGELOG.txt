0.2.0
-----

* Ported to Python 3.

0.1.5
-----

* changed the board position notation to match George Bell's.

0.1.4
-----

* fix peg selection bug when selection state is mislaid after a
  rotation or flip.
* more fiddling with themes -- can be switched on command line.

0.1.3
-----

* added board rotation and flipping to the engine and gui, for
  additional excitement. 
* store winning moves with less redundancy (only once for any set of
  transpositions). 
* make show hints sticky across games.
* new color theme.
